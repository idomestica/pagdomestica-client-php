<?php

// ini_set('display_errors', true);

require('../../helpers.php');

$title = 'Cadastrar Funcionário';

if (isset($_REQUEST['id_funcionario'])) {
  $values = funcionario($_REQUEST['id_funcionario']);
  $action = 'funcionario-put.php';
} else {
  $action = 'funcionario-post.php';
}

?>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title><?php echo $title; ?></title>
<link rel="stylesheet" type="text/css" href="funcionario.css">
</head>

<body>

<form method="post" action="<?php echo $action; ?>">
  <h1><?php echo $title; ?></h1>
  <fieldset>
    <div>
      <label for="id_empregador">Empregador</label>
      <select id="id_empregador" name="id_empregador">
        <?php echo empregador_options($values->id_empregador); ?>
      </select>
    </div>
    <div>
      <label for="nome">Nome</label>
      <input id="nome" name="nome" type="text" value="<?php echo $values->nome; ?>">
    </div>
    <div>
      <label for="data_de_admissao">Data de Admissão</label>
      <input id="data_de_admissao" name="data_de_admissao" type="text" value="<?php echo $values->data_de_admissao; ?>">
    </div>
    <div>
      <label for="id_cargo">Cargo</label>
      <select id="id_cargo" name="id_cargo">
        <?php echo cargo_options($values->id_cargo); ?>
      </select>
    </div>
    <div>
      <label for="salario">Salário</label>
      <input id="salario" name="salario" type="text" value="<?php echo $values->salario; ?>">
    </div>
    <div>
      <label for="ctps">CTPS / Série / UF</label>
      <input id="ctps" name="ctps" type="text" value="<?php echo $values->ctps; ?>"> /
      <input id="ctps_serie" name="ctps_serie" type="text" value="<?php echo $values->ctps_serie; ?>"> /
      <select id="ctps_uf" name="ctps_uf">
        <?php echo options(estado(), 'uf', 'uf', $values->ctps_uf); ?>
      </select>
    </div>
    <div>
      <label for="nit">NIT</label>
      <input id="nit" name="nit" type="text" value="<?php echo $values->nit; ?>">
    </div>
    <div>
      <label for="trabalho_uf">Local de Trabalho</label>
      <select id="trabalho_uf" name="trabalho_uf">
        <?php echo options(estado(), 'uf', 'uf', $values->trabalho_uf); ?>
      </select>
      <select id="id_local_de_trabalho" name="id_local_de_trabalho">
        <?php echo local_de_trabalho_options($values->id_local_de_trabalho); ?>
      </select>
    </div>
    <div>
      <label for="vt">Vale Transporte / Valor / Dias</label>
      <input id="vt" name="vt" type="checkbox"<?php echo checked($values->vt); ?>> /
      <input id="vt_valor" name="vt_valor" type="text" value="<?php echo $values->vt_valor; ?>"> /
      <select id="vt_dias" name="vt_dias">
        <option>Nenhum</option>
        <option value="{1,2,3,4,5}">Segunda à Sexta</option>
        <option value="{1,3,5}">Segunda, Quarta e Sexta</option>
        <option value="{2,4}">Terça e Quinta</option>
      </select>
    </div>
    <div>
      <label for="vt_nao_descontar">Não descontar Vale Transporte</label>
      <input id="vt_nao_descontar" name="vt_nao_descontar" type="checkbox"<?php echo checked($values->vt_nao_descontar); ?>>
    </div>
    <div>
      <label for="fgts">FGTS / Data</label>
      <input id="fgts" name="fgts" type="checkbox"<?php echo checked($values->fgts); ?>> /
      <input id="fgts_data" name="fgts_data" type="text" value="<?php echo $values->fgts_data; ?>">
    </div>
    <div>
      <label for="inss_nao_descontar">Não descontar INSS</label>
      <input id="inss_nao_descontar" name="inss_nao_descontar" type="checkbox"<?php echo checked($values->inss_nao_descontar); ?>>
    </div>
    <div>
      <label for="experiencia_dias">Experiência / Prorrogação</label>
      <input id="experiencia_dias" name="experiencia_dias" type="text" value="<?php echo $values->experiencia_dias; ?>"> /
      <input id="experiencia_prorrogacao" name="experiencia_prorrogacao" type="text" value="<?php echo $values->experiencia_prorrogacao; ?>">
    </div>
    <div>
      <label for="dependentes">Número de Dependentes</label>
      <input id="dependentes" name="dependentes" type="text" value="<?php echo $values->dependentes; ?>">
    </div>
    <div>
      <label for="cpf">CPF</label>
      <input id="cpf" name="cpf" type="text" value="<?php echo $values->cpf; ?>">
    </div>
    <div>
      <label for="sexo">Sexo</label>
      <select id="sexo" name="sexo">
        <option value="F">Feminino</option>
        <option value="M">Masculino</option>
      </select>
    </div>
    <div>
      <label for="data_de_nascimento">Data de Nascimento</label>
      <input id="data_de_nascimento" name="data_de_nascimento" type="text" value="<?php echo $values->data_de_nascimento; ?>">
    </div>
    <div>
      <label for="logradouro">Logradouro / Número / Complemento</label>
      <input id="logradouro" name="logradouro" type="text" value="<?php echo $values->logradouro; ?>"> /
      <input id="numero" name="numero" type="text" value="<?php echo $values->numero; ?>"> /
      <input id="complemento" name="complemento" type="text" value="<?php echo $values->complemento; ?>">
    </div>
    <div>
      <label for="bairro">Bairro</label>
      <input id="bairro" name="bairro" type="text" value="<?php echo $values->bairro; ?>">
    </div>
    <div>
      <label for="cidade">Cidade / Estado / CEP</label>
      <input id="cidade" name="cidade" type="text" value="<?php echo $values->cidade; ?>"> /
      <select id="uf" name="uf">
        <?php echo options(estado(), 'uf', 'uf', $values->uf); ?>
      </select> /
      <input id="cep" name="cep" type="text" value="<?php echo $values->cep; ?>">
    </div>
    <div>
      <label for="ddd_celular">DDD / Celular / Operadora</label>
      <input id="ddd_celular" name="ddd_celular" type="text" value="<?php echo $values->ddd_celular; ?>"> /
      <input id="numero_celular" name="numero_celular" type="text" value="<?php echo $values->numero_celular; ?>"> /
      <select id="id_operadora" name="id_operadora">
        <?php echo operadora_options($values->id_operadora); ?>
      </select>
    </div>
    <div>
      <label for="ddd_residencial">DDD / Telefone Residencial</label>
      <input id="ddd_residencial" name="ddd_residencial" type="text" value="<?php echo $values->ddd_residencial; ?>"> /
      <input id="numero_residencial" name="numero_residencial" type="text" value="<?php echo $values->numero_residencial; ?>">
    </div>
  </fieldset>
  <input type="submit" value="Enviar">
  <input type="hidden" name="id_funcionario" value="<?php echo $_REQUEST['id_funcionario']; ?>"
</form>

</body>

</html>