<?php

class Client {
  private $host;
  private $user = null;
  private $pwd = null;
  
  function __construct($host, $user = null, $pwd = null) {
    $this->host = $host;
    $this->user = $user;
    $this->pwd = $pwd;
  }
  
  private function url($url = null) {
    $_host = rtrim($this->host, '/');
    $_url = ltrim($url, '/');
    
    return "{$_host}/{$_url}";
  }
  
  private function url_query_string($url, $params) {
    $qs = array();
    if ($params) {
      foreach ($params as $key => $value) {
        $qs[] = "{$key}=" . urlencode($value);
      }
    }
    
    $url = explode('?', $url);
    if ($url[1]) $url_qs = $url[1];
    $url = $url[0];
    if ($url_qs) $url = "{$url}?{$url_qs}";
    
    if (count($qs)) return "{$url}?" . implode('&', $qs);
    else return $url;
  }
  
  //private function encode_params(&$params) {
  //  foreach($params as $key => $value) {
  //    $params[$key] = urlencode($value);
  //  }
  //}
  
  private function request($verb, $url, $params = null) {
    $ch = curl_init();
    $url = $this->url($url);
    
    switch($verb) {
      case 'GET':
      case 'DELETE':
        $url = $this->url_query_string($url, $params);
    }
    
    curl_setopt($ch, CURLOPT_URL, $url);
    if ($this->user and $this->pwd) {
      curl_setopt($ch, CURLOPT_USERPWD, "{$this->user}:{$this->pwd}");
    }
    
    switch ($verb) {
      case 'POST':
        curl_setopt($ch, CURLOPT_POST, true);
        break;
      case 'PUT':
      case 'DELETE':
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $verb);
    }
    
    switch ($verb) {
      case 'POST':
      case 'PUT':
        //$this->encode_params($params);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
    }
    
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    
    $response = curl_exec($ch);
    curl_close($ch);
    
    return $response;
  }
  
  public function get($url, $params = null) {
    return $this->request('GET', $url, $params);
  }
  
  public function post($url, $params = null) {
    return $this->request('POST', $url, $params);
  }
  
  public function put($url, $params = null) {
    return $this->request('PUT', $url, $params);
  }
  
  public function delete($url, $params = null) {
    return $this->request('DELETE', $url, $params);
  }
}

?>
