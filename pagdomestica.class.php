<?php

require('client.class.php');
require('pagdomestica.conf.php');

class PagDomestica extends Client {
  function __construct() {
    global $conf;
    return parent::__construct($conf['host'], $conf['user'], $conf['pwd']);
  }
}

?>