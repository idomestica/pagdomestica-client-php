<?php


require('pagdomestica.class.php');

$pd = new PagDomestica();




function options($rows, $value, $content, $selectedValue = null) {
  if ($selectedValue === '') {
    $selected = ' selected="selected"';
  }
  $options = "<option{$selected}></option>";
  if ($rows) {
    foreach($rows as $row) {
      $selected = '';
      if ($selectedValue != null) {
        if ($row->$value == $selectedValue) {
          $selected = ' selected="selected"';
        }
      }
      $options .= '<option value="' . $row->$value . '"' . $selected . '>' . $row->$content . '</option>';
    }
  }
  return $options;
}


function checked($value) {
  if ($value == 't') {
    return ' checked="checked"';
  } else {
    return '';
  }
}




function empregador($id = null) {
  global $pd;
  
  if ($id === null) {
    $response = $pd->get('empregador');
    $json = json_decode($response);
    return $json->rows;
  } else {
    $response = $pd->get("empregador/{$id}");
    $json = json_decode($response);
    return $json->rows[0];
  }
}

function empregador_options($selectedValue) {
  return options(empregador(), 'id_empregador', 'nome', $selectedValue);
}


function funcionario($id = null) {
  global $pd;
  
  if ($id === null) {
    $response = $pd->get("funcionario");
    $json = json_decode($response);
    return $json->rows;
  } else {
    $response = $pd->get("funcionario/{$id}");
    $json = json_decode($response);
    return $json->rows[0];
  }
}

function funcionario_options($selectedValue) {
  return options(funcionario(), 'id_funcionario', 'nome', $selectedValue);
}


function cargo() {
  global $pd;
  
  $response = $pd->get('cargo');
  $json = json_decode($response);
  
  return $json->rows;
}

function cargo_options($selectedValue = null) {
  return options(cargo(), 'id_cargo', 'nome', $selectedValue);
}


function local_de_trabalho() {
  global $pd;
  
  $response = $pd->get('local-de-trabalho');
  $json = json_decode($response);
  
  return $json->rows;
}

function local_de_trabalho_options($selectedValue = null) {
  return options(local_de_trabalho(), 'id_local_de_trabalho', 'nome', $selectedValue);
}


function estado() {
  global $pd;
  
  $response = $pd->get('estado');
  $json = json_decode($response);
  
  return $json->rows;
}

function estado_options($selectedValue = null) {
  return options(estado(), 'id_estado', 'nome', $selectedValue);
}


function operadora() {
  global $pd;
  
  $response = $pd->get('operadora');
  $json = json_decode($response);
  
  return $json->rows;
}

function operadora_options($selectedValue = null) {
  return options(operadora(), 'id_operadora', 'nome', $selectedValue);
}


?>